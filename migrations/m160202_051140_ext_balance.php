<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_051140_ext_balance extends Migration
{
    public function up()
    {
        $this->createTable('ext_balance', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'value' => $this->string()->notNull()->defaultValue(0),
            'link' => $this->string()->notNull(),
            'currency' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('ext_balance');
    }


}
