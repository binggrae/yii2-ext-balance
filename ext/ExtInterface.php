<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 15:59
 */

namespace altiger\balance\ext;


interface ExtInterface
{


    public function install();

    public function load();

}