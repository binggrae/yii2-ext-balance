<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.08.2017
 * Time: 12:02
 */

namespace altiger\balance\ext;


use altiger\balance\models\ExtBalance;
use yii\httpclient\Client;

class FineProxy extends ExtClass implements ExtInterface
{

    public $slug = 'fineproxy';

    public $label = 'Прокси';

    public $link = 'https://account.fineproxy.org/';

    public $currency = 'ч.';

    public $login; //UA_NA210082

    public $password; //KnZram3TvG


    public function load()
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('https://account.fineproxy.org/')
            ->setHeaders([
                'origin' => 'https://account.fineproxy.org',
                'referer' => 'https://account.fineproxy.org/',
                'user-agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
            ])
            ->setData([
                'log' => $this->login,
                'pass' => $this->password,
                'logsub' => 'Войти',
            ])
            ->send();
        if ($response->isOk) {
            preg_match('/\[ Осталось (\d+) д\. (\d+) ч\. (\d+) мин\. до удаления аккаунта \]/', $response->content, $math);

            if (count($math) > 1) {

                $days = $math[1] . 'д. ' . $math[2];

                ExtBalance::updateAll(['value' => $days], ['slug' => $this->slug]);
            }
        }
    }

}