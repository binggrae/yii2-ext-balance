<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.12.2016
 * Time: 2:19
 */

namespace altiger\balance\ext;


use altiger\balance\models\ExtBalance;
use yii\base\Object;

class ExtClass extends Object
{
    public $slug;

    public $label;

    public $link;

    public $currency;

    public $login;

    public $password;

    public $params;


    public function install()
    {
        if (!ExtBalance::find()->bySlug($this->slug)->one()) {
            $model = new ExtBalance($this->getNewData());
            $model->save();

            \Yii::$app->getModule('settings');

            \Yii::$app->settings->set($this->slug, 100, \Yii::$app->extBalance->settings_key, 'integer');

        }
    }


    public function load()
    {

    }

    public function getNewData()
    {
        return [
            'label' => $this->label,
            'slug' => $this->slug,
            'value' => '',
            'link' => $this->link,
            'currency' => $this->currency,
        ];
    }

}