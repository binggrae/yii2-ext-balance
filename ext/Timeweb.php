<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 15:48
 */

namespace altiger\balance\ext;


use altiger\balance\models\ExtBalance;
use yii\httpclient\Client;

class Timeweb extends ExtClass implements ExtInterface
{
    public $slug = 'timeweb';

    public $label = 'Хостинг';

    public $link = 'https://vds.timeweb.ru/servers';

    public $currency = 'руб.';

    public $login;

    public $password;


    public function load()
    {
        $balance = '---';
        $cookie = \Yii::getAlias('@console/data/cookie.txt');
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $csrfResponse = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://vds.timeweb.ru/login')
            ->setOptions([
                CURLOPT_COOKIEJAR => $cookie,
                CURLOPT_COOKIEFILE => $cookie,
            ])
            ->setHeaders([
                'origin' => 'https://vds.timeweb.ru',
                'referer' => 'https://vds.timeweb.ru/login',
                'user-agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
            ])
            ->send();
        if ($csrfResponse->isOk) {
            preg_match('/\<input type="hidden" name="_csrf" value="([a-zA-Z0-9\=\.\/]+)"/', $csrfResponse->content, $math);
            if (isset($math[1])) {
                $loginResponse = $client->createRequest()
                    ->setMethod('post')
                    ->setHeaders([
                        'origin' => 'https://vds.timeweb.ru',
                        'referer' => 'https://vds.timeweb.ru/login',
                        'user-agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                        'x-requested-with' => 'XMLHttpRequest',
                        'content-type' => 'application/json',
                        'accept' => 'application/json, text/javascript, */*; q=0.01'
                    ])
                    ->setUrl('https://vds.timeweb.ru/login')
                    ->setOptions([
                        CURLOPT_COOKIEJAR => $cookie,
                        CURLOPT_COOKIEFILE => $cookie,
                    ])
                    ->setData([
                        '_csrf' => $math[1],
                        'LoginForm[panelType]' => 'vds',
                        'LoginForm[username]' => $this->login,
                        'LoginForm[password]' => $this->password,
                        'LoginForm[rememberMe]' => '1',
                    ])
                    ->send();

                if ($loginResponse->data['type'] == 'success') {
                    $resultResponse = $client->createRequest()
                        ->setMethod('get')
                        ->setUrl('https:' . $loginResponse->data['url'])
                        ->setOptions([
                            CURLOPT_COOKIEJAR => $cookie,
                            CURLOPT_COOKIEFILE => $cookie,
                        ])
                        ->setHeaders([
                            'origin' => 'https://vds.timeweb.ru',
                            'referer' => 'https://vds.timeweb.ru/login',
                            'user-agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
                        ])
                        ->send();

                    preg_match('/class="js-account-balance">\s*(\d+)?\s*</', $resultResponse->content, $math);
                    if (isset($math[1])) {
                        $balance = $math[1];
                    } 
                } 
            } 
        } 
        ExtBalance::updateAll(['value' => $balance], ['slug' => $this->slug]);
    }



}