<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 15:48
 */

namespace altiger\balance\ext;


use altiger\balance\models\ExtBalance;
use yii\base\Object;
use yii\httpclient\Client;

class Infosmska extends ExtClass implements ExtInterface
{
    public $slug = 'infosmska';

    public $label = 'CMC';

    public $link = 'https://www.infosmska.ru/Pages/Cabinet/Default.aspx';

    public $currency = 'руб.';

    public $login;

    public $password;


    public function load()
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('http://api.infosmska.ru/interfaces/getbalance.ashx')
            ->setData([
                'login' => $this->login,
                'pwd' => $this->password,
            ])
            ->send();

        if ($response->isOk) {
            ExtBalance::updateAll(['value' => $response->content], ['slug' => $this->slug]);
        }

    }


}