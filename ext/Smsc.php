<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 15:48
 */

namespace altiger\balance\ext;


use altiger\balance\models\ExtBalance;
use yii\base\Object;
use yii\httpclient\Client;

class Smsc extends ExtClass implements ExtInterface
{
    public $slug = 'smsc';

    public $label = 'CMC МИР';

    public $link = 'https://smsc.ru/user/';

    public $currency = 'руб.';

    public $login;

    public $password;


    public function load()
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('https://smsc.ru/sys/balance.php')
            ->setData([
                'login' => $this->login,
                'psw' => $this->password,
                'fmt' => 3,
            ])
            ->send();

        if ($response->isOk) {
            ExtBalance::updateAll(['value' => $response->data['balance']], ['slug' => $this->slug]);
        }

    }


}