<?php

namespace altiger\balance\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "balance".
 *
 * @property integer $id
 * @property string $label
 * @property string $slug
 * @property string $value
 * @property string $link
 * @property string $currency
 */
class ExtBalance extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'ext_balance';
    }

    public static function find()
    {
        return new ExtBalanceQuery(get_called_class());
    }


    public function rules()
    {
        return [
            [['label', 'slug', 'link', 'currency'], 'required'],
            [['label', 'slug', 'value', 'link', 'currency'], 'string', 'max' => 255]
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'slug' => 'Slug',
            'value' => 'Value',
            'link' => 'Link',
            'currency' => 'Currency',
        ];
    }
}

class ExtBalanceQuery extends ActiveQuery
{

    /**
     * @param string $name
     * @return ExtBalanceQuery
     */
    public function bySlug($name)
    {
        $t = ExtBalance::tableName();

        $this->andWhere(["{$t}.slug" => $name]);

        return $this;
    }

}
