<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 15:43
 */

namespace altiger\balance;


use altiger\balance\ext\ExtClass;
use yii\base\Component;

class ExtBalance extends Component
{
    public $ext;

    public $settings_key = 'balance';

    public $default_danger_value = 100;

    public $role = 'manager';

    public function install()
    {
        foreach ($this->ext as $ext) {
            /** @var ExtClass $class */
            $class = new $ext['class']($ext['params']);
            $class->install();
        }

    }
	public function load()
    {
        foreach ($this->ext as $ext) {
            /** @var ExtClass $class */
            $class = new $ext['class']($ext['params']);
            $class->load();
        }

    }


}