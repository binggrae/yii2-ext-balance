Usage <a name="usage"></a>
---

Add repository
```php
"repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/binggrae/yii2-ext-balance.git"
    }
  ],
```

Installation is recommended to be done via [composer][] by running:

    composer require --prefer-dist altiger/yii2-ext-balance


Add component
```php
'extBalance' => [
    'class' => 'altiger\balance\ExtBalance',
    'settings_key' => 'balance',
    'default_danger_value' => 100,
    'role' => 'manager',
    'ext' => [
        'timeweb' => [
            'class' => 'altiger\balance\ext\Timeweb',
            'params' => [
                'slug' => 'timeweb',
                'label' => 'Хостинг',
                'link' => 'https://vds.timeweb.ru/servers',
                'currency' => 'руб.',
                'login' => 'login',
                'password' => 'password'
            ],
        ]
    ]
],
```


Allowed sites

    infosmska => altiger\balance\ext\Infosmska
    timeweb => altiger\balance\ext\Timeweb


---

Apply migration

    php yii migrate --migrationPath=@vendor/altiger/yii2-ext-balance/migrations  --interactive=1

---

Usage

- Controller

        \Yii::$app->extBalance->install(); // install sites

        \Yii::$app->extBalance->load(); // load balance

- Menu

        use altiger\balance\widgets\ExtBalanceMenu;
        <?php echo ExtBalanceMenu::widget(); ?>

