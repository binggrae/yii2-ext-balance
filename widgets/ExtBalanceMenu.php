<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.12.2016
 * Time: 18:39
 */

namespace altiger\balance\widgets;


use altiger\balance\models\ExtBalance;
use frontend\widgets\Menu;

class ExtBalanceMenu extends Menu
{
    public $linkTemplate = "<a href=\"{url}\" target=\"_blank\">\n{icon}\n<span>{label}</span>\n{right-icon}\n{badge}</a>";


    public function init()
    {

        $this->items = [[
            'label' => 'Баланс',
            'options' => ['class' => 'header'],
            'visible' => \Yii::$app->user->can(\Yii::$app->extBalance->role)
        ]];

        /** @var ExtBalance[] $models */
        $models = ExtBalance::find()->all();

        foreach ($models as $model) {
            $value = $model->value;
            $badgeBgClass = intval($value) > \Yii::$app->settings->get(
                $model->slug, \Yii::$app->extBalance->settings_key, \Yii::$app->extBalance->default_danger_value
            ) ? 'success' : 'danger';
            $this->items[] = [
                'label' => $model->label,
                'url' => $model->link,
                'icon' => '<i class="fa fa-money"></i>',
                'badge' => $model->value . ' ' . $model->currency,
                'badgeBgClass' => 'label-' . $badgeBgClass,
                'visible' => \Yii::$app->user->can(\Yii::$app->extBalance->role)
            ];
        }
    }

}